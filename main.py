from matplotlib import pyplot as plt
import numpy as np

R1 = 0.2
R2 = 0.67
k = 0.7
w = 2
x = np.pi / 4
theta = np.linspace(-5, 10, 256)

A1 = []
A2 = []


def amplitude(t):

    A1.append((R1 * np.cos(k*x - w*t) + (1 - R1) * np.cos(k*x + w*t)))
    A2.append((R2 * np.cos(k*x*2 - (w-0.5)*t) + (1 - R2) * np.cos(k*x*2 + (w-0.5)*t)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    time = -5
    while time < 10:
        amplitude(time)
        time += 15/256

    plt.arrow(0, -1.5, 0, 3, linewidth=0.5, head_width=0.2, head_length=0.08, color='black')
    plt.text(0, 1.7, 'A')
    plt.arrow(-6, 0, 18, 0, linewidth=0.5, head_width=0.05, head_length=0.3, color='black')
    plt.text(13, 0, 'θ')
    plt.plot(theta, A1, linewidth=0.75)
    plt.plot(theta, A2, '--', linewidth=0.75)
    plt.axis('off')

    plt.show()

